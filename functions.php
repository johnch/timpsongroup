<?php
function timpson_log($entry) {
  $log = TEMPLATEPATH."/log.txt";
  if($log) {
    file_put_contents($log, "\n".date('d/m/y H:i:s').': '.json_encode($entry), FILE_APPEND);
  }
}
function enqueueTimpson() {
  wp_enqueue_script( 'jquery-masonry', array( 'jquery' ) );
  wp_enqueue_script( 'script', get_template_directory_uri() . '/js/min/scripts-min.js', array('jquery'),'1.0',true);
}
add_action( 'wp_enqueue_scripts', 'enqueueTimpson' );

foreach (glob(TEMPLATEPATH."/functions/*.php") as $filename) {
  include $filename;
}