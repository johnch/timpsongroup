<?php
/**
 * The Header for the Timpson Group WP theme
 */
?>

<!doctype html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="shortcut icon" href="<?php echo esc_url(get_theme_mod('favicon')) ; ?>" />
  <title><?php wp_title( '|', true, 'right' ); ?></title>
  <script type="text/javascript">var masonry = 1;</script>
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/style.css" />
  <style><?php echo timpson_generate_css(); ?></style>
  <!--[if lt IE 9]>
  <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
  <?php wp_head(); ?>
</head>
  
<body ontouchstart="">
  <div id="wrap">
  <header class="section">
    <div class="row">
      <?php if ( get_theme_mod( 'logo' ) ) : ?>
      <div class='logo'>
          <a href='<?php echo esc_url( home_url( '/' ) ); ?>' title='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>' rel='home'>
            <img src='<?php echo esc_url( get_theme_mod( 'logo' ) ); ?>' alt='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>'>
            <h1 class="hide"><?php bloginfo('description'); ?></h1>
          </a>
      </div>
      <?php else : ?>
      <hgroup>
          <h1 class='site-title'><a href='<?php echo esc_url( home_url( '/' ) ); ?>' title='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>' rel='home'><?php bloginfo( 'name' ); ?></a></h1>
          <h2 class='site-description'><?php bloginfo( 'description' ); ?></h2>
      </hgroup>
      <?php endif; ?>
    </div>
    <nav class="row topnav">
    <?php clean_top_menu('nav-primary'); ?>
    <?php icon_menu('nav-icons'); ?>
    </nav>
  </header>
  
  <section class="section">
    <div class="row masonry" style="margin-bottom: 20px">