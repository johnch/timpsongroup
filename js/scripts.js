jQuery(document).ready(function(){

  function imageLoaded() {
    counter--; 
    if( counter === 0 ) {
      if(masonry) {
        var $container = jQuery('.masonry');
        console.log(jQuery($container.get().reverse()));
        jQuery($container.get().reverse()).masonry({
          columnWidth: function( containerWidth ) {
            return containerWidth / 4;
          }
        });
        jQuery(window).trigger('resize');
      } else {
        jQuery('body').addClass('no-masonry');
      }
    }
  }

  var images = jQuery('img');
  var counter = images.length;  // initialize the counter
  console.log(counter);
  images.each(function() {
    if( this.complete ) {
      imageLoaded.call( this );
    } else {
      jQuery(this).one('load', imageLoaded);
    }
  });
});