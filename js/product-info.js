function get_product(permalink) {
  return jQuery.ajax({
  url: productAJAX.ajaxurl,
  dataType: 'json',
  data: ({action : 'get_product', permalink: permalink}),
  });
}

function set_product(data, permalink, carousel, size) {

  console.log(data);
  var id = $('[data-hash='+permalink+']').attr('data-id');
   carousel.trigger('to.owl.carousel', id);
  $('#name').html(data.name);
  if (data.from == 1) {$('#price').html('from '+data.price);} else {$('#price').html(data.price);}
  $('#tabPhotosC').html(data.images);
  for (var i = 0; i < 4; i++) {
    var j = i+1;
    var tab = data.tabs[i];
    if (tab.show == 1) {
      if(!($('#tab'+j).length)) {
        $('#tabNav ul').append('<li id="tab'+j+'">'+tab.name+'</li>');
        $('#tabsC').append('<div class="tab" id="tab'+j+'C">'+tab.content+'</div>');
      }
    } else {if($('#tab'+j).length) {$('#tab'+j+', #tab'+j+'C').remove();}}
  }
  if(size == 'small') {
    var num = $('#tabNav ul li').length;
    var width = ((100/num)+'%');
    $('#tabNav ul li').css('width', width);
  }
}

function set_current(hash, data, hashlist) {
  return data[hashlist[hash]];
}

jQuery(document).ready(function(){
  $ = jQuery.noConflict();

  var current = products[0];

  var hash = (window.location.hash).substring(1);
  if !(current.slug == hash) {
    set_current(hash, products, hashlist);
  }

  console.log(products_json);

  var promise = get_product(hash);
  promise.success(function(data) {
    set_product(data, hash);
  });

  $('.owl-item > div').click(function() {
    var permalink = $(this).attr('data-hash');
    var promise = get_product(permalink);
    promise.success(function(data) {
      set_product(data, permalink);
    });
  });
});