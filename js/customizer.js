function upload_font(id) {
  var fileInput = document.getElementById(id);
  var fontFile = fileInput.files[0];
  var formData = new FormData();
  formData.append('timpson_custom_font', fontFile);
  formData.append('action', 'timpson_upload_custom_font');

  return jQuery.ajax({
    url: customizerAJAX.ajaxurl,
    type: 'POST',
    data: formData,
    processData: false,
    contentType: false
  });
}

function preview_font(data, id) {
  // Update live preview css
  var fontFace = id.replace("timpson_","").replace("_","-");
  wp.customize(fontFace, function(obj) {
    obj.set(data);
  } );
  var iFrame = jQuery('#customize-preview > iframe');
  var style = iFrame.contents().find('#'+fontFace+"-face");
  var css = style.text();
  css = css.replace(/url\(\"[\w-,:]*/g,'url(\"'+data);
  style.text(css);

}

jQuery(document).ready(function(){
  //console.log("Custom JS Loaded");
  jQuery(".timpson_reset_font").click(function(){
    var id = jQuery(this).attr('data-id');
    var data = 'none';
    preview_font(data, id);
  });
  jQuery(".timpson_custom_font").change(function (){
    //console.log("Change Registered");
    var id = jQuery(this).attr('id');
    console.log(id);
    var promise = upload_font(id);
    promise.success(function(data) {
      preview_font(data, id);
      // document.getElementById(id).setAttribute("value", data);
      // var save = document.getElementById('save')
      // save.setAttribute("value", "Save & Publish!");
      // save.removeAttribute("disabled");
    });
  });
});