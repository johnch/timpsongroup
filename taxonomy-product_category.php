<?php
if (!function_exists('woocommerce_content')) {
// Get products by category
do_action('product_setup');

$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );

$solr = new wp_Solr;
$solr = $solr->client;
$search_term = "product_category_str:".$term->name;
$query = $solr->createSelect();
$query->setQuery( $search_term );
$resultset = $solr->select($query);

$products = $resultset->getDocuments();

$i=0;
foreach ($products as $product) {
  $hashlist_json[$product->slug] = $i;
}
$hashlist_json = json_encode($hashlist_json);
$products_json = json_encode($resultset->getData());

get_header(); ?>
  <script type="text/javascript">var products=<?php echo $products_json; ?>; var hash_list=<?php echo $hashlist_json ?>;</script>
  <section class="section">
    <div class="row">
      <div class="col c1-1 short">
        <h1><?php echo ucfirst($term->name); ?></h1>
      </div>
      <div class="col c1-1 short">
      <?php
          if ( $resultset->getNumFound() > 0 ) { ?>
        <h1><span id="name"><?php echo $products[0]->title; ?></span> it costs <span id="price"><?php echo $products[0]->product_price_str[0] ?></span></h1>
        <button>Create Now</button>
      </div>
      <div id="tabs" class="col c1-1">
        <nav id="tabNav" class="tabs">
          <ul>
            <li id="tabPhotos">Photos</li>
            <?php if($products[0]->product_tab_1_name_str[0] != '') { ?><li id="tab1"><?php echo $products[0]->product_tab_1_name_str[0]; ?></li><?php } ?>
            <?php if($products[0]->product_tab_2_name_str[0] != '') { ?><li id="tab1"><?php echo $products[0]->product_tab_2_name_str[0]; ?></li><?php } ?>
            <?php if($products[0]->product_tab_3_name_str[0] != '') { ?><li id="tab1"><?php echo $products[0]->product_tab_3_name_str[0]; ?></li><?php } ?>
            <?php if($products[0]->product_tab_4_name_str[0] != '') { ?><li id="tab1"><?php echo $products[0]->product_tab_4_name_str[0]; ?></li><?php } ?>
          </ul>
        </nav>
        <div id="tabsC" class="tile tabbed">
          <?php if($products[0]->product_tab_1_name_str[0] != '') { ?><div id="tab1C"><?php echo $products[0]->product_tab_1_content_str[0]; ?></div><?php } ?>
          <?php if($products[0]->product_tab_2_name_str[0] != '') { ?><div id="tab1C"><?php echo $products[0]->product_tab_2_content_str[0]; ?></div><?php } ?>
          <?php if($products[0]->product_tab_3_name_str[0] != '') { ?><div id="tab1C"><?php echo $products[0]->product_tab_3_content_str[0]; ?></div><?php } ?>
          <?php if($products[0]->product_tab_4_name_str[0] != '') { ?><div id="tab1C"><?php echo $products[0]->product_tab_4_content_str[0]; ?></div><?php } ?>
        </div>
      </div>
      <?php } ?>
    </div>
  </section>
<?php 
get_footer(); 
}
?>