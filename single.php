

<?php
get_header(); ?>
    <div class="col c1-4 no-gutter no-pad" id="blog-sidebar"><?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Blog Sidebar') ) : endif; ?></div>
    <section class="col c3-4 no-pad masonry" style="margin-top:0">
      <?php while ( have_posts() ) : the_post(); ?>
      <a href="<?php echo( site_url().'/blog/' ); ?>" class="col c1-1"><---- Back to Blog </a>
        <h2 class="col c1-1"><?php the_title(); ?></h2>
        <?php the_content();
      endwhile; // end of the loop. ?>
    </section>
<?php get_footer(); ?>