<?php
/**** GENERATE CLEAN MENUS ****/
// Luckily my menu generator works for this site with very few changes!

function register_my_menus() {
  register_nav_menus(
    array( 'nav-primary' => __( 'Site Navigation' ), 'nav-icons' => __('Secondary Nav') )
  );
}
add_action( 'init', 'register_my_menus' );

function get_menu_children($menu_name, $parent){
  if (($locations = get_nav_menu_locations()) && $locations[$menu_name] > 0) {
    $menu = wp_get_nav_menu_object($locations[$menu_name]);
    $menu_items = wp_get_nav_menu_items($menu->term_id);
    $submenu = array(); 

    foreach($menu_items as $item){
        if($item->menu_item_parent == $parent)
          array_push($submenu, $item);
        }

    if ($submenu) { // if we found any
      return $submenu;
    } else {
      return null;
    }
  }
}

function icon_menu($slug) {
  /*$menu_name = $slug; // specify custom menu slug
  if (($locations = get_nav_menu_locations()) && $locations[$menu_name] > 0) {
    $menu_items = get_menu_children($menu_name, 0);

    $menu_list = "<nav class='icons'>";
      foreach ((array) $menu_items as $key => $menu_item) {
        $title = $menu_item->title;
        $url = $menu_item->url;
        $id = $menu_item->db_id;
        $target = $menu_item->target;
        $icon_name = $menu_item->attr_title;
        $menu_list .= '<div><a href="'. $url .'" target="'.$target.'"><span class="icon-'.$icon_name.'"><span class="hide">'. $title . '</span></a></div>';
      }
    $menu_list .= "</nav>";
  } else {
    $menu_list = '<!-- no list defined -->';
  }
  echo $menu->term_id;
  echo $menu_list;*/
  echo <<<EOT
    <nav class="icons">
      <a href="http://www.maxphoto.co.uk/store-locator/" target="">
        <span class="icon-storelocator">
          <span class="hide">Store Locator</span>
        </span>
      </a>
    </nav>
EOT;
} 

function clean_top_menu($slug) {
  $menu_name = $slug; // specify custom menu slug
  if (($locations = get_nav_menu_locations()) && isset($locations[$menu_name])) {
    $menu_items = get_menu_children($menu_name, 0);

    $menu_list = "<ul>";
      foreach ((array) $menu_items as $key => $menu_item) {
        $title = $menu_item->title;
        $url = $menu_item->url;
        $id = $menu_item->db_id;
        $target = $menu_item->target;
        $submenu = get_menu_children($menu_name, $id);
        if (!$submenu) {
          $menu_list .= '<li><a href="'. $url .'" target="'.$target.'">'. $title . '</a></li>';
        } else {
          $menu_list .= '<li>'. $title .'<ul class="nav-drop">';
          foreach ($submenu as $key => $child) {
            $child_title = $child->title;
            $child_url = $child->url;
            $child_id = $child->db_id;
            $child_target = $child->target;
            $subsubmenu = get_menu_children($menu_name, $child_id);
            if (!$subsubmenu) {
              $menu_list .= '<li><a href="'.$child_url.'" target="'.$child_target.'">'.$child_title.'</a></li>';
            } else {
              $menu_list .= '<li>'.$child_title.'<ul class="sub-nav-drop">';
              foreach ($subsubmenu as $key => $sub_child) {
                $sub_child_title = $sub_child->title;
                $sub_child_url = $sub_child->url;
                $sub_child_target = $sub_child->target;
                $menu_list .= '<li><a href="'.$sub_child_url.'" target="'.$sub_child_target.'">'.$sub_child_title.'</a></li>';
              }
              $menu_list .= '</ul></li>';
            }
          }
          $menu_list .= '</ul></li>';
        }
      }
    $menu_list .= "</ul>";

  } else {
    $menu_list = '<!-- no list defined -->';
  }
  echo $menu->term_id;
  echo $menu_list;
};