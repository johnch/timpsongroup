<?php
// On save of Customizer generate custom css option.
function timpson_generate_css() {
	$mods = array('headings_font', 'body_font', 'horizontal_gutter', 'vertical_gutter', 'base_colour', 'nav_colour_1', 'nav-colour-2', 'tagline_position', 'bg_colour', 'tile_radius', 'overlay_svg', 'header_text_colour');
	foreach ($mods as $mod) {
		$mods[$mod] = get_theme_mod($mod);
	}
  if (($mods['headings_font'] != 'none') AND ($mods['headings_font'] != '')) {
  	$css = <<<EOT
      @font-face{font-family:headings_font;src:url($mods[headings_font].eot);src:url($mods[headings_font].eot?#iefix) format('embedded-opentype'),url($mods[headings_font].woff2) format('woff2'),url($mods[headings_font].woff) format('woff'),url($mods[headings_font].ttf) format('truetype');font-weight:400;font-style:normal}.button,.hc button.btn,.hc input[type=submit],button,h1,h2,input[type=submit],nav,nav div span{font-family:headings_font}
EOT;
  } else $css = '.button,.hc button.btn,.hc input[type=submit],button,h1,h2,input[type=submit],nav,nav div span{font-family:"Arial Rounded MT Bold","Helvetica Rounded",Arial,sans-serif}';
  
  if (($mods['body_font'] != 'none') AND ($mods['body_font'] != '')) {
    $css .= <<<EOT
      @font-face{font-family:body_font;src:url($mods[body_font].eot);src:url($mods[body_font].eot?#iefix) format('embedded-opentype'),url($mods[body_font].woff2) format('woff2'),url($mods[body_font].woff) format('woff'),url($mods[body_font].ttf) format('truetype');font-weight:400;font-style:normal}*,body,html{font-family:body_font}
EOT;
  } // Currently using the 'default default' so no else.

  $marginWidth = $mods['horizontal_gutter'];
  $half_marginWidth = $marginWidth/2;
  // Widths
  $three_four = 75 - $marginWidth;
  $four = 25 - $marginWidth;
  $two = 50 - $marginWidth;
  $one = 100 - $marginWidth;
  if($mods['tagline_position'] == 'bottom') { $padding = 'padding-top: 50px;'; } 
    elseif($mods['tagline_position'] == 'top') { $padding = 'padding-bottom: 50px;'; } 
      else { $padding = ''; };

  $css .= <<<EOT
    .col{margin-top:$mods[vertical_gutter]px;margin-left:$half_marginWidth%}.col.c1-1{width:$one%}.col.c1-2{width:$two%}.col.c1-4{width:$four%}.col.c3-4{width:$three_four%}@media screen and (max-width:480px){.col.c1-1{width:100%}.col.c1-2,.col.c3-4{width:100%;margin-left:0}.col.c1-4{width:$two%}}footer,header{background-color:$mods[base_colour]}.nav-drop{background-color:$mods[nav_colour_1]}.nav-drop li,.sub-nav-drop li,nav li:active,nav li:focus,nav li:hover{background:$mods[nav_colour_2];filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#0070a0', endColorstr='#004b8d', GradientType=0)}@media screen and (max-width:480px){.nav-drop>li:active,.nav-drop>li:focus,.nav-drop>li:hover,.sub-nav-drop{background-color:$mods[nav_colour_2]}}.slick-slide img,body{background-color:$mods[bg_colour]}.tile .overlay{background:url($mods[overlay_svg]) 0 0/100% 100%}.tile{border-radius:$mods[tile_radius]px}.icons a,.logo h1,.nav-drop>li a,.overlay a,.sub-nav-drop>li a,.topnav>ul>li>a,footer,header{color:$mods[header_text_colour]}.logo h1{$padding}
EOT;
  return $css;
}