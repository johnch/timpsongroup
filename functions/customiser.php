<?php
/*** MISC FUNCTIONS ***/

// Fix SVG Uploads
add_filter('upload_mimes', 'custom_upload_mimes');
function custom_upload_mimes ( $existing_mimes=array() ) {
  $existing_mimes['svg'] = 'mime/type';
  return $existing_mimes;
}

/**** TIMPSON THEME CUSTOMISER ****/

add_action('customize_register', 'timpson_customize');
function timpson_customize($wp_customize) {

  /** Classes **/
  class Timpson_Customize_Number_Control extends WP_Customize_Control {
    public $type = 'number';
    public function render_content() { ?>
      <label>
        <span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
        <input type="number" step="1" min="0" <?php $this->link(); ?> value="<?php echo intval( $this->value() ); ?>" />
      </label>
      <?php 
    }
  }/** Classes **/
  class Timpson_Customize_Small_Number_Control extends WP_Customize_Control {
    public $type = 'number';
    public function render_content() { ?>
      <label>
        <span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
        <input type="number" step="0.01" min="0" <?php $this->link(); ?> value="<?php echo intval( $this->value() ); ?>" />
      </label>
      <?php 
    }
  }
  class Timpson_Customize_Font_Control extends WP_Customize_Upload_Control {
    public $type = 'font';
    public function enqueue() {
      wp_register_script( "customizer-script", get_template_directory_uri().'/js/min/customizer-min.js', array('jquery') );
      wp_localize_script( 'customizer-script', 'customizerAJAX', array( 'ajaxurl' => admin_url( 'admin-ajax.php' )));
      wp_enqueue_script( 'customizer-script');
    }
    public function render_content() {
      ?>
      <label><?php echo esc_html( $this->label ); ?></label>
      <span class="customize-control-title"><?php echo esc_html( $this->description ); ?></span>
      <input type="file" value="<?php echo esc_html($this->value); ?>" class="timpson_custom_font" id="<?php echo esc_html( $this->id ); ?>" name="timpson_custom_font" accept="zip" <?php echo esc_html($this->get_link()); ?>>
      <input type="button" data-id="<?php echo esc_html( $this->id ); ?>" class="timpson_reset_font">
      <?php
    }
  }

  /*** LOGO UPLOAD ***/ 
  $wp_customize->add_section( 'logo_settings', array(
    'title'          => 'Logo',
    'priority'       => 35,
  ) );

  $wp_customize->add_setting( 'logo' );
  $wp_customize->add_setting( 'favicon' );
  $wp_customize->add_setting( 'tagline_position' );

  $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'timpson_logo', array(
    'label'    => __( 'Logo', 'Timpson' ),
    'section'  => 'logo_settings',
    'settings' => 'logo',
  ) ) );

  $wp_customize->add_control('timspon_tagline_position', array(
    'label' => __( 'Tagline Position', 'Timpson' ),
    'section' => 'logo_settings',
    'settings' => 'tagline_position',
    'type' => 'radio',
    'choices' => array('top' => 'Top', 'center' => 'Centre', 'bottom' => 'Bottom'),
  ) );

  $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'timpson_favicon', array(
    'label'    => __( 'Favicon', 'Timpson' ),
    'section'  => 'logo_settings',
    'settings' => 'favicon',
  ) ) );

  /*** SET COLOURS ***/
  $wp_customize->add_section( 'colour_settings', array(
    'title'          => 'Colours',
    'priority'       => 40,
  ) );

  $wp_customize->add_setting( 'base_colour', array('default' => '#004B8D'));
  $wp_customize->add_setting( 'bg_colour', array('default' => '#E2F0F9'));
  $wp_customize->add_setting( 'header_text_colour', array('default' => 'white'));
  $wp_customize->add_setting( 'nav_colour_1', array('default' => '#004B8D'));
  $wp_customize->add_setting( 'nav_colour_2', array('default' => '#004B8D'));

  $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'timpson_base_colour', array(
    'label'    => __( 'Base Colour', 'Timpson' ),
    'section'  => 'colour_settings',
    'settings' => 'base_colour',
  ) ) );
  $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'timpson_bg_colour', array(
    'label'    => __( 'Background Colour', 'Timpson' ),
    'section'  => 'colour_settings',
    'settings' => 'bg_colour',
  ) ) );
  $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'timpson_header_text_colour', array(
    'label'    => __( 'Header Text Colour', 'Timpson' ),
    'section'  => 'colour_settings',
    'settings' => 'header_text_colour',
  ) ) );
  $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'timpson_nav_colour_1', array(
    'label'    => __( 'Nav Drop Colour 1', 'Timpson' ),
    'section'  => 'colour_settings',
    'settings' => 'nav_colour_1',
  ) ) );
  $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'timpson_nav_colour_2', array(
    'label'    => __( 'Nav Drop Colour 2', 'Timpson' ),
    'section'  => 'colour_settings',
    'settings' => 'nav_colour_2',
  ) ) );

  // Fonts

  $wp_customize->add_section( 'font_settings', array(
    'title'          => 'Fonts',
    'priority'       => 45,
  ) );

  $wp_customize->add_setting( 'headings_font', array('transport' => 'postMessage', 'default' => 'none') );
  $wp_customize->add_setting( 'body_font', array('transport' => 'postMessage', 'default' => 'none') );

  $wp_customize->add_control( new Timpson_Customize_Font_Control( $wp_customize, 'timpson_headings_font', array(
    'label'    => __( 'Font', 'Timpson' ),
    'section'  => 'font_settings',
    'settings' => 'headings_font',
  ) ) );

  $wp_customize->add_control( new Timpson_Customize_Font_Control( $wp_customize, 'timpson_body_font', array(
    'label'    => __( 'Font', 'Timpson' ),
    'section'  => 'font_settings',
    'settings' => 'body_font',
  ) ) );

  // Overlay svg - y/n, url
  $wp_customize->add_section( 'overlay_settings', array(
    'title'          => 'Overlay',
    'priority'       => 45,
  ) );

  $wp_customize->add_setting( 'overlay_svg' );
  $wp_customize->add_setting( 'overlay_position' );

  $wp_customize->add_control( new WP_Customize_Upload_Control( $wp_customize, 'timpson_overlay_svg', array(
    'label'    => __( 'Overlay SVG', 'Timpson' ),
    'section'  => 'overlay_settings',
    'settings' => 'overlay_svg',
  ) ) );

  $wp_customize->add_control('timpson_overlay_position', array(
    'label'    => __( 'Overlay SVG', 'Timpson' ),
    'section'  => 'overlay_settings',
    'settings' => 'overlay_position',
    'type'     => 'radio',
    'choices'  => array('header' => 'Header', 'footer' => 'Footer', null => 'None'),
  ) );

  // Layout

  $wp_customize->add_section('layout_settings', array(
    'title' => 'Layout',
    'priority' => 45
  ) );

  $wp_customize->add_setting('tile_radius', array('default' => 0));
  $wp_customize->add_setting('vertical_gutter', array('default' => 20));
  $wp_customize->add_setting('horizontal_gutter', array('default' => 1.25));

  $wp_customize->add_control( new Timpson_Customize_Number_Control( $wp_customize, 'timpson_tile_radius', array(
    'label'    => __( 'Tile Corner Radius', 'Timpson' ),
    'section'  => 'layout_settings',
    'settings' => 'tile_radius',
  ) ) );
  $wp_customize->add_control( new Timpson_Customize_Number_Control( $wp_customize, 'timpson_vertical_gutter', array(
    'label'    => __( 'Vertical Spacing', 'Timpson' ),
    'section'  => 'layout_settings',
    'settings' => 'vertical_gutter',
  ) ) );
  $wp_customize->add_control( new Timpson_Customize_Small_Number_Control( $wp_customize, 'timpson_horizontal_gutter', array(
    'label'    => __( 'Horizontal Spacing', 'Timpson' ),
    'section'  => 'layout_settings',
    'settings' => 'horizontal_gutter',
  ) ) );
}