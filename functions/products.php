<?php
if (!function_exists('woocommerce_content')) {
/**** AJAX GET PRODUCTS ****/

add_action( 'product_setup', 'timpson_product_enqueuer' );

function timpson_product_enqueuer() {
   wp_register_script( "product_script", get_template_directory_uri().'/js/product-info.js', array('jquery') );
   wp_localize_script( 'product_script', 'productAJAX', array( 'ajaxurl' => admin_url( 'admin-ajax.php' )));   
   wp_enqueue_script( 'product_script' );
   wp_enqueue_script('carousel_script', get_template_directory_uri().'owlcarousel/owl.carousel.js');
   wp_enqueue_style('carousel_style', get_template_directory_uri().'owlcarousel/assets/owl.carousel.css');
}

function timpson_get_product() {
  // Pull product photos from db, all the rest will be provided by Solarium as JSON on page load.
}
add_action("wp_ajax_nopriv_get_product", "timpson_get_product");
add_action("wp_ajax_get_product", "timpson_get_product");

/**** ADMIN PAGES ****/
add_theme_support( 'post-thumbnails', array( 'post', 'product' ) ); 
// Register Custom Post Type
function timpson_custom_post_product() {
  $labels = array(
    'name'               => _x( 'Products', 'post type general name' ),
    'singular_name'      => _x( 'Product', 'post type singular name' ),
    'add_new'            => _x( 'Add New', 'book' ),
    'add_new_item'       => __( 'Add New Product' ),
    'edit_item'          => __( 'Edit Product' ),
    'new_item'           => __( 'New Product' ),
    'all_items'          => __( 'All Products' ),
    'view_item'          => __( 'View Product' ),
    'search_items'       => __( 'Search Products' ),
    'not_found'          => __( 'No products found' ),
    'not_found_in_trash' => __( 'No products found in the Trash' ), 
    'parent_item_colon'  => '',
    'menu_name'          => 'Products'
  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'Holds our products and product specific data',
    'public'        => true,
    'menu_position' => 5,
    'supports'      => array( 'title', 'thumbnail' ),
    'has_archive'   => false,
  );
  register_post_type( 'product', $args );
}
add_action( 'init', 'timpson_custom_post_product' );

// Register Custom Taxonomy
function timpson_taxonomies_product() {
  $labels = array(
    'name'              => _x( 'Product Categories', 'taxonomy general name' ),
    'singular_name'     => _x( 'Product Category', 'taxonomy singular name' ),
    'search_items'      => __( 'Search Product Categories' ),
    'all_items'         => __( 'All Product Categories' ),
    'parent_item'       => __( 'Parent Product Category' ),
    'parent_item_colon' => __( 'Parent Product Category:' ),
    'edit_item'         => __( 'Edit Product Category' ), 
    'update_item'       => __( 'Update Product Category' ),
    'add_new_item'      => __( 'Add New Product Category' ),
    'new_item_name'     => __( 'New Product Category' ),
    'menu_name'         => __( 'Product Categories' ),
  );
  $args = array(
    'labels' => $labels,
    'hierarchical' => true,
    'slug' => 'products'
  );
  register_taxonomy( 'product_category', 'product', $args );
}
add_action( 'init', 'timpson_taxonomies_product', 0 );
}