<?php
// Callback function to insert 'styleselect' into the $buttons array
function timpson_mce_buttons_2( $buttons ) {
  array_unshift( $buttons, 'styleselect' );
  return $buttons;
}
add_editor_style();
// Register our callback to the appropriate filter
add_filter('mce_buttons_2', 'timpson_mce_buttons_2');


/**** TINYMCE CUSTOMISATION ****/

// Add new styles to the TinyMCE "formats" menu dropdown
if ( ! function_exists( 'wpex_styles_dropdown' ) ) {
  function wpex_styles_dropdown( $settings ) {

    // Create array of new styles
    $new_styles = array(
      array(
        'title' => __( 'Columns', 'wpex' ),
        'items' => array(
          array(
            'title'   => __('Full width','wpex'),
            'block' => 'div',
            'classes' => 'col c1-1',
            'wrapper' => true,
          ),
          array(
            'title'   => __('1/2 width','wpex'),
            'block' => 'div',
            'classes' => 'col c1-2',
            'wrapper' => true,
          ),
          array(
            'title'   => __('1/4 width','wpex'),
            'block' => 'div',
            'classes' => 'col c1-4',
            'wrapper' => true,
          )
        )),
      array(
        'title' => __( 'Misc', 'wpex' ),
        'items' => array(
          array(
            'title'   => __('Tile','wpex'),
            'selector'  => "[class*='col c']",
            'classes' => 'tile',
            'wrapper' => true,
          ),
          array(
            'title'   => __('No Gutter','wpex'),
            'selector'  => ".col",
            'classes' => 'no-gutter',
            'wrapper' => true,
          ),
          array(
            'title'   => __('No Padding','wpex'),
            'selector'  => ".tile",
            'classes' => 'no-pad',
            'wrapper' => true,
          )
        )
      ),
    );

    // Merge old & new styles
    $settings['style_formats_merge'] = true;

    // Add new styles
    $settings['style_formats'] = json_encode( $new_styles );

    // Return New Settings
    return $settings;

  }
}
add_filter( 'tiny_mce_before_init', 'wpex_styles_dropdown' );
