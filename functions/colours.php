<?php
/**
* Change the brightness of the passed in color
*
* $diff should be negative to go darker, positive to go lighter and
* is subtracted from the decimal (0-255) value of the color
*
* @param string $hex color to be modified
* @param string $diff amount to change the color
* @return string hex color
* @author Alex King - http://alexking.org
*/
function hex_color_mod($hex, $diff) {
  $rgb = str_split(trim($hex, '# '), 2);
   
  foreach ($rgb as &$hex) {
  $dec = hexdec($hex);
  if ($diff >= 0) {
  $dec += $diff;
  }
  else {
  $dec -= abs($diff); 
  }
  $dec = max(0, min(255, $dec));
  $hex = str_pad(dechex($dec), 2, '0', STR_PAD_LEFT);
  }
   
  return '#'.implode($rgb);
}