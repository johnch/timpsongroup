<?php
// Add category metabox to page
function categories_for_all() {
	register_taxonomy_for_object_type('category', 'page');
}
add_action( 'admin_init', 'categories_for_all' );