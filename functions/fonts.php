<?php
/**
 * WP Customizer Extension for FontSquirrel Uploader
 */
function timpson_upload_custom_font() {
  $font = $_FILES['timpson_custom_font'];
  // timpson_log('Font recieved:');
  // timpson_log($font);
  // Make sure the file array isn't empty
  if(!empty($font['name'])) {
       
      // Setup the array of supported file types. In this case, it's just PDF.
      $supported_types = array("application/zip");
       
      // Get the file type of the upload
      $arr_file_type = wp_check_filetype(basename($font['name']));
      $uploaded_type = $arr_file_type['type'];
      // timpson_log("Uploaded type is: ".$uploaded_type);
      // Check if the type is supported. If not, throw an error.
      if(in_array($uploaded_type, $supported_types)) {
        // Use the WordPress API to upload the file
        $upload = wp_upload_bits($font['name'], null, file_get_contents($font['tmp_name']));
 
        if(isset($upload['error']) && $upload['error'] != 0) {
            wp_die('There was an error uploading your file. The error is: ' . $upload['error']);
        } else {
          WP_Filesystem();
          $destination = wp_upload_dir();
          $destination_path = $destination['path'];
          // Use PHP to unzip as WP's unzip_file seems to fail without giving any errors
          $zip = new ZipArchive;
          $res = $zip->open($destination_path.'/'.$font['name']);
          if ($res === TRUE) {
            $zip->extractTo($destination['basedir'].'/fonts/');
            $filename = $destination['basedir'].'/fonts/'.$zip->getNameIndex(2);
            $zip->close();
            $filename = strstr($filename, '.', true);
            $filename = strstr($filename, '/wp-content/');
            echo esc_url(home_url($filename));
            // timpson_log('Successfully unzipped the file!'); 
          } else {
            echo "There was an error opening the file";
            // timpson_log('There was an error opening the file.');       
          } 
        } // end if/else
      } else {
          echo "Please choose a zip file.";
          // timpson_log("The file type that you've uploaded is not a .zip.");
      } // end if/else
       
  } else {
    echo "No file uploaded.";
    // timpson_log("Nothing in files.");
  } // end if
  die();
}
add_action("wp_ajax_nopriv_timpson_upload_custom_font", "timpson_upload_custom_font");
add_action("wp_ajax_timpson_upload_custom_font", "timpson_upload_custom_font");
