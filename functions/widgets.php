<?php
/**** FOOTER WIDGET AREAS ****/

if (function_exists('register_sidebar')) {
  register_sidebar(array(
    'name' => 'Footer Widget 1',
    'id'   => 'footer-widget-1'
  ));
  register_sidebar(array(
    'name' => 'Footer Widget 2',
    'id'   => 'footer-widget-2'
  ));
  register_sidebar(array(
    'name' => 'Blog Sidebar',
    'id'   => 'blog-sidebar'
  ));
}