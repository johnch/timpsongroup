<?php
if (!function_exists('woocommerce_content')) {
/*  array( 'box name' => array( 'Title', 
*                               'location', 
*                               'priority', 
*                               array(  'field1' => array('Label',
*                                                         'type',
*                                                         array('attribute' => 'value')), 
*                                       'field2' => array(etc) )
*   )); */

$timpson_metaboxes = array( 
  'price' => array( 
    'Price',
    'side',
    'high',
    array(
      'price_from' => array(
        'Show from?<br>',
        'radio',
        array(
          'values' => array('yes','no'),
          'labels' => array('Yes','No')
        )
      ),
      'price' => array( 
        '',
        'number',
        array(
          'required' => 'required',
          'min' => 0, 
          'step' => 0.01
        )
      ),
      'offer_price' => array( 
        'Offer Price:<br>',
        'number',
        array(
          'required' => 'required', 
          'min' => 0, 
          'step' => 0.01
        ),
      ),
    ),
  ),
  'tab_1' => array( 
    'Tab 1', 
    'normal', 
    'high',
    array(
      'tab_1_name' => array(
        '',
        'text',
        array(
          'placeholder' => 'Tab 1 Name'
        )
      ),
      'tab_1_content' => array(
        '',
        'editor'
      )
    )
  ),
  'tab_2' => array( 
    'Tab 2', 
    'normal', 
    'high',
    array(
      'tab_2_name' => array(
        '',
        'text',
        array(
          'placeholder' => 'Tab 2 Name'
        )
      ),
      'tab_2_content' => array(
        '',
        'editor'
      )
    )
  ),
  'tab_3' => array( 
    'Tab 3', 
    'normal', 
    'high',
    array(
      'tab_3_name' => array(
        '',
        'text',
        array(
          'placeholder' => 'Tab 3 Name'
        )
      ),
      'tab_3_content' => array(
        '',
        'editor'
      )
    )
  ),
  'tab_4' => array( 
    'Tab 4', 
    'normal', 
    'high',
    array(
      'tab_4_name' => array(
        '',
        'text',
        array(
          'placeholder' => 'Tab 4 Name'
        )
      ),
      'tab_4_content' => array(
        '',
        'editor'
      )
    )
  ),
  'display' => array(
    'Display on Site', 
    'normal', 
    'default',
    array(
      'display' => array(
        '',
        'radio',
        array(
          'values' => array('yes','no'),
          'labels' => array('Yes','No')
        )
      )
    )
  ),
  'delivery_time' => array(
    'Delivery Time', 
    'side', 
    'default',
    array(
      'delivery_time' => array( 
        '',
        'number',
        array(
          'required' => 'required', 
          'min' => 1, 
          'step' => 1
        )
      ),
      'free_delivery' => array(
        'Free Delivery?<br>',
        'radio',
        array(
          'values' => array('yes','no'),
          'labels' => array('Yes','No')
        )
      )
    )
  ),
);

function product_meta_boxes() {
  global $timpson_metaboxes;

  foreach ($timpson_metaboxes as $box => $config) {
    add_meta_box( 
      'product_'.$box.'_box',
      __( $config[0], 'Timpson Product' ),
      'product_box_content',
      'product',
      $config[1],
      $config[2],
      $config[3]
    );
  }
}
add_action( 'add_meta_boxes_product', 'product_meta_boxes' );

function product_box_content( $post, $metabox ) {
  $fields = $metabox['args'];
  wp_nonce_field( basename( __FILE__ ), 'product_box_content_nonce' );

  foreach ($fields as $field => $data) {
    $id = 'product_'.$field;
    $value = esc_attr(get_post_meta($post->ID, $id, true));
    if ($data[1] == 'editor'){ 
      wp_editor($value, $id); 
      return;
    }

    if ($data[0] != '') $out = '<label for="product_'.$field.'">'.$data[0].'</label>';

    if ($data[1] == 'radio') {
      for ($i=0; $i < count($data[2]['values']); $i++) { 
        $out .= '<label for="'.$id.'_'.$data[2]['values'][$i].'">'.$data[2]['labels'][$i].' </label>';
        $out .= '<input type="radio" id="'.$id.'_'.$data[2]['values'][$i].'" name="'.$id.'" value="'.$data[2]['values'][$i].'" ';
        if ($data[2]['values'][$i] == $value) $out .= 'checked="checked"';
        $out .= ' >';
      }
    } else {
      $out .= '<input type="'.$data[1].'" id="'.$id.'" name="'.$id.'" value="'.$value.'" ';
      foreach ($data[2] as $key => $value) {
        $out .= $key.'="'.$value.'" ';
      }
      $out .= '><br>';
    }
    echo $out;
  }
}

function product_meta_boxes_save( $post_id ) {
  if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
  if ( ($_POST['post_type'] != 'product') || !current_user_can( 'edit_post', $post_id )) return;

  global $timpson_metaboxes;
  foreach ($timpson_metaboxes as $box => $config) {
    if ( !wp_verify_nonce( $_POST['product_box_content_nonce'], basename( __FILE__ ) ) ) return;
    foreach($config[3] as $field => $value) {
      $id = 'product_'.$field;
      $value = $_POST[$id];
      update_post_meta( $post_id, $id, $value );
    }
  }
}
add_action( 'save_post', 'product_meta_boxes_save' );
}