  
    </div>
  </section>
  <footer class="section no-masonry">
    <div class="row">
      <div class="col c3-4"><?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer Widget 1') ) : endif; ?></div>
      <div class="col c1-4" id="copyright"><?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer Widget 2') ) : endif; ?></div>
      
    </div>
    <?php wp_footer() ?>
    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-25003854-2', 'auto');
  ga('send', 'pageview');

</script>
  </footer>
  </div> <!-- close wrapper -->
  </body>
</html>